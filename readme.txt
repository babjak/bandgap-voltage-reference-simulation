﻿Design of a bandgap voltage reference circuit, which provides a stable,
temperature insensitive voltage level.
=========================
Bandgap voltage reference
=========================
---------------------
Benjamin Babjak, 2010
---------------------


Abstract
========

This document briefly explains the design of a bandgap voltage reference circuit, which provides a stable, temperature insensitive voltage level. 


The problem
===========

Given a well-known bandgap voltage reference circuit [1]_ , where: 

1. VDD = 3.3V,
2. 2907 BJT,
3. LT1994 operational amplifier,
4. CMOS body connected to source.

The task is to have a Vout (V+ minus V- at the opamp outputs) of 2 volts +/- 5mV and a tempco of less than 10 uV/degree C. All resistors either #k or #.5k, no higher resolution than that is allowed. Also, no W/L values at higher resolution than 0.5. There is no restriction on bipolar n or the op-amp common-mode voltage.


Design process
==============

Parameters and initial setup
----------------------------

Since it was not specified I decided to start with CMOS transistors of the same size :math:`\frac{W}{L} = \frac{50\text{um}}{0.5\text{um}}`. The common mode input voltage of the differential amplifier was chosen to be 2.3V. Initially all the resistors were chosen as shown in the paper [1]_ with the exception of :math:`R_1` which was 1kohm. I've found that a higher value here eventually results in a significantly higher resistance at :math:`R_0`. These somewhat random values were the results of trial and error method of problem solving.


Bipolar Core
------------

First I had to make sure the Bipolar Core operates as expected. The constraints here were 

1. :math:`I_{R_2} = I_{R_2}` because of the current mirror structure,
2. :math:`V_H = V_N` because of the PTAT current concept.

Using the SPICE directive ".step param n 0.2 3 0.2" I've swept through the range of :math:`n=0.2` to :math:`n=3` with a step size of 0.2 looking for the minimum of :math:`I_{R_2} - I_{R_2}` and :math:`V_H - V_N`, which I've found at :math:`n=2.6816` eventually. Temperature was set with ".temp 17".

Next I've optimized for the bandgap reference voltage just above :math:`R_0` named :math:`V_B` on my schematic with the constraint here being temperature independence. I've used the directives ".STEP TEMP 17 27 10" and ".step param R0 10000 30000 500" to sweep through various values of temperatures and resistances respectively. I was looking for the resistance at which :math:`V_B` stays the same for both temperature values and ended up with :math:`R_0 = 12 \text{kohm}`.


Differential reference
----------------------

With the bipolar core working I've started optimizing the differential reference. First I've tried to leave the :math:`\frac{W}{L}` and the common-mode voltage unchanged and only tune the resistor values. I've managed to meet and eventually exceed the specifications, however, through hours and hours of simulation I've found that with the 0.5kohm resolution constraint I can't even get close. Thus I had to optimize :math:`\frac{W}{L}` and the common-mode voltage as well. 

One important issue here was to realize that with :math:`R_1` and :math:`R_6` fixed the connection between :math:`R_4` and :math:`R_5` is linear.

.. math::
	\begin{aligned}
		V_{out} & = & \frac{R_4}{R_6} V_{BE4} + 2 \frac{R_5}{R_1} V_T \ln n 
	\end{aligned}

The optimization process was the following:

1. Chose a resistor value for :math:`R_6` so that :math:`V_Q \approx V_U` (important design choice) and possibly :math:`V_{out} \approx 2\text{V}`,
2. Sweep through multiple :math:`R_4` and :math:`R_5` values and find the right combinations that result in :math:`V_{out} = 2\text{V}`,
3. Use the above results to find the linear connection between :math:`R_4` and :math:`R_5` (linear regression), 
4. Pick :math:`R_4` and :math:`R_5` value pairs that don't violate the linear connection too much while simultaneously satisfy the 0.5 kohm constraint,
5. Sweep through multiple :math:`W` and :math:`L` values for :math:`M_{11}` and :math:`M_9` to find the lowest temperature dependance for :math:`V_{out}`,
6. Repeat.

After several iterations I've ended up with results that met the specifications. :math:`R_6 = 10.5 \text{kohm}`, :math:`R_5 = 19 \text{kohm}`, :math:`R_4 = 18.5 \text{kohm}`, and :math:`\frac{W}{L} = \frac{249\text{um}}{0.5\text{um}}`.

At this point the final optimization involved tuning the common-mode voltage with respect to the temperature dependence. I've found that the common-mode voltage did not have a significant effect on :math:`V_{out}`, however at this point the temperature dependence was so small, that even the common-mode voltage was of importance. The common-mode voltage was 2.507V.


.. figure:: figures/Voltage_Reference.png
   :align: center
   :width: 100%
   
   Bandgap reference. 


Simulation results
------------------

Simulation gives a :math:`V_{out}=1.99939 \text{V}` that does not seem to change as the temperature changes, so the tempco is effectively 0 uV/degree C. As for the supply voltage change :math:`V_{out}=2.00087 \text{V}` if the supply voltage is :math:`3.63\text{V}`, thus :math:`PSR = \frac{2.00087 \text{V} - 1.99939 \text{V}}{2\text{V}} \frac{1}{3.63\text{V} - 3.3 \text{V}} = \frac{1.48 \text{uV}}{2 \text{V}} \frac{1}{0.33 \text{V}} = 2242.42 \frac{\text{ppm}}{\text{V}}`, which is significantly worse than the temperature dependence.



.. [1] T. L. Brooks and A. L. Westwick, A low-power differential CMOS bandgap reference, vol. 26. IEEE, 1994, pp. 248–249.
